describe Gitlab::Homepage::Team do
  describe '#members' do
    it 'returns an array of team members' do
      expect(subject.members).to all(be_a Gitlab::Homepage::Team::Member)
    end
  end

  describe '#projects' do
    it 'returns a list of projects with members assigned' do
      expect(subject.projects.first.key).to eq 'gitlab-ce'
      expect(subject.projects.first.maintainers.count).to be_nonzero
    end
  end

  describe 'integration with YAML data sources' do
    describe 'first team member validations' do
      let(:member) { subject.members.first }

      it 'correctly integrates with data/team.yml' do
        expect(member.username).to eq 'dzaporozhets'
        expect(member.country).to eq 'Ukraine'
        expect(member.story).to match /Dmitriy started GitLab/
        expect(member.roles).to be_a Hash
        expect(member.assignments.first.project.name)
          .to eq 'GitLab Community Edition (CE)'
      end
    end

    describe 'first project validations' do
      let(:project) { subject.projects.first }

      it 'correctly integrates with data/projects.yml' do
        expect(project.name). to eq 'GitLab Community Edition (CE)'
        expect(project.assignments.first.username).to eq 'dzaporozhets'
      end
    end

    describe 'dangling assignments validation' do
      let(:members) { subject.members }
      let(:projects) { subject.projects }

      it 'does not assign non-existent projects to anyone' do
        members.each do |member|
          member.roles.each_key do |key|
            expect(key).to satisfy('be a defined project key') do |k|
              projects.any? { |project| k == project.key }
            end
          end
        end
      end
    end
  end
end
